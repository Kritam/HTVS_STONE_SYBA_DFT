# High Throughput Workflow for the Computational Design of New Thermally Activated Delayed Fluorescence Emitters

## Overview
This repository contains Jupyter Notebook files that form the computational workflow for the generation, mutation, and analysis of molecules library related to my PhD titled "High Throughput Workflow for the Computational Design of New Thermally Activated Delayed Fluorescence Emitters." The work presented here was conducted as part of my doctoral research at Imperial College London.

### Thesis Abstract
_This thesis explores the use of computational methods for discovering new TADF molecules, with a focus on developing a high-throughput virtual screening workflow that reduces costs and time associated with experimental screening. Using methods like STONED and SYBA, diverse molecule libraries were generated and evaluated to identify promising candidates for further investigation. The study also examines the challenges of using computational methods, such as discrepancies and limitations with computationally efficient methods. Modifications were made to parent molecules based on $\Delta$SCF calculations and similarity map analysis. Overall, this study provides valuable insights into the use of computational methods for TADF molecule design and offers guidance for future research aimed at designing new TADF materials._

## Project Description
In my PhD work, I developed a high throughput workflow for the computational design of new thermally activated delayed fluorescence (TADF) emitters. This repository contains Jupyter Notebook files that detail the steps involved in generating the molecules library, performing mutations, and analyzing the results.

## Prerequisite ##
RDkit - https://www.rdkit.org/docs/Install.html

SELFIES -  https://github.com/aspuru-guzik-group/selfies

SYBA - https://github.com/lich-uct/syba

## Usage
The repository is organised into different Jupyter Notebook files, each representing a specific step in the workflow. Follow the notebooks in the numerical order (Part_1_Molecule_generator_exhaustive_search.ipynp, Part_2_Analysis_unique_mols_and_FP_scores_single_run.ipynp, etc.) to perform the entire workflow from molecule generation to mutation and analysis.

Please refer to the notebook files themselves for detailed explanations, code examples, and comments that guide you through each step.

## Documentation
For more comprehensive documentation and theoretical background, please refer to my PhD thesis titled "High Throughput Workflow for the Computational Design of New Thermally Activated Delayed Fluorescence Emitters."

## Contributing
Thank you for your interest in contributing to this project! If you find any issues or have suggestions for improvements, please feel free to open a GitHub issue or submit a pull request.

## Contact
If you have any questions or need further assistance, feel free to contact me:

- Email: kt4218@ic.ac.uk
